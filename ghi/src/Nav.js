// import NavItem from "./NavItem";
import { NavLink } from "react-router-dom";
function Nav() {
  return (
    <header>
      <nav className='navbar navbar-expand-lg navbar-light bg-light'>
        <div className='container-fluid'>
          <NavLink className='navbar-brand' to=''>
            Conference GO!
          </NavLink>
          <button
            className='navbar-toggler'
            type='button'
            data-bs-toggle='collapse'
            data-bs-target='#navbarSupportedContent'
            aria-controls='navbarSupportedContent'
            aria-expanded='false'
            aria-label='Toggle navigation'
          >
            <span className='navbar-toggler-icon'></span>
          </button>
          <div
            className='collapse navbar-collapse'
            id='navbarSupportedContent'
          >
            <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
              <li className='nav-item'>
                <NavLink className='nav-link' aria-current='page' to=''>
                  Home
                </NavLink>
              </li>
              <li className='nav-item'>
                <NavLink
                  className='nav-link'
                  id='locationNavId'
                  aria-current='page'
                  to='/locations/new'
                >
                  New location
                </NavLink>
              </li>
              <li className='nav-item'>
                <NavLink
                  className='nav-link'
                  id='conferenceNavId'
                  aria-current='page'
                  to='/conferences/new'
                >
                  New conference
                </NavLink>
              </li>
              <li className='nav-item'>
                <NavLink
                  className='nav-link'
                  id='presentationNavId'
                  aria-current='page'
                  to='/presentations/new'
                >
                  New presentation
                </NavLink>
              </li>
              <li className='nav-item'>
                <NavLink
                  className='nav-link'
                  id='conferenceListNav'
                  aria-current='page'
                  to='/attendees'
                >
                  Attendee List
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
}

export default Nav;

// function Nav() {
//     return (
//       <header>
//         <nav className="navbar navbar-expand-lg navbar-light bg-light">
//           <div className="container-fluid">
//             <a className="navbar-brand" href="http://localhost:3000">Conference GO!</a>
//             {/* <button
//               className="navbar-toggler"
//               type="button"
//               data-bs-toggle="collapse"
//               data-bs-target="#navbarSupportedContent"
//               aria-controls="navbarSupportedContent"
//               aria-expanded="false"
//               aria-label="Toggle navigation"
//             >
//               <span className="navbar-toggler-icon"></span>
//             </button> */}
//             <div className="collapse navbar-collapse" id="navbarSupportedContent">
//               <ul className="navbar-nav me-auto mb-2 mb-lg-0">
//                 <li className="nav-item">
//                   <a className="nav-link active" aria-current="page" href="http://localhost:3000">Home</a>
//                 </li>
//                 <li className="nav-item">
//                   <a
//                     className="nav-link"
//                     id="locationNavId"
//                     aria-current="page"
//                     href="new-location.html"
//                     >New location</a
//                   >
//                 </li>
//                 <li className="nav-item">
//                   <a
//                     className="nav-link"
//                     id="conferenceNavId"
//                     aria-current="page"
//                     href="new-conference.html"
//                     >New conference</a
//                   >
//                 </li>
//                 <li className="nav-item">
//                   <a
//                     className="nav-link"
//                     id="presentationNavId"
//                     aria-current="page"
//                     href="new-presentation.html"
//                     >New presentation</a
//                   >
//                 </li>
//                 <li className="nav-item">
//                   <a
//                     className="nav-link"
//                     aria-current="page"
//                     id="conferenceListId"
//                     href="http://localhost:3001"
//                   >
//                     Conference List
//                   </a>
//                 </li>
//               </ul>
//   {/* <form className="d-flex">
//     <input
//       className="form-control me-2"
//       type="search"
//       placeholder="Search conferences"
//       aria-label="Search"
//     />
//     <button className="btn btn-outline-success me-2" type="submit">
//       Search
//     </button>
//     <a className="btn btn-primary" href="attend-conference.html"
//       >Attend!</a
//     >
//   </form> */}
//             </div>
//           </div>
//         </nav>
//       </header>
//     );
//   }

//   export default Nav;
