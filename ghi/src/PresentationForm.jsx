import React, { useState, useEffect } from "react";

const PresentationForm = () => {
  const initialformData = {
    presenter_name: "",
    presenter_email: "",
    company_name: "",
    title: "",
    synopsis: "",
    conferences: [],
  };
  const [conferences, setConferences] = useState([]);
  const [formData, setFormData] = useState(...initialformData);
  const getData = async () => {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const { conference } = formData;

    const presentationURL = `http://localhost:8000/api/conferences/${conference}/presentations/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: { "Content-Type": "application/json" },
    };
    const response = await fetch(presentationURL, fetchConfig);
    if (response.ok) {
      setFormData(...initialformData);
    }
  };
  const handleInputChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  return (
    <div className='row'>
      <div className='offset-3 col-6'>
        <div className='shadow p-4 mt-4'>
          <h1>Create a new presentation</h1>
          <form onSubmit={handleSubmit} id='create-presentation-form'>
            <div className='form-floating mb-3'>
              <input
                onChange={handleInputChange}
                value={formData.presenter_name}
                placeholder='presenter_name'
                required
                type='text'
                name='presenter_name'
                id='presenter_name'
                className='form-control'
              />
              <label htmlFor='presenter_name'>Presenter name</label>
            </div>
            <div className='form-floating mb-3'>
              <input
                onChange={handleInputChange}
                value={formData.presenter_email}
                placeholder='presenter_email'
                required
                type='email'
                name='presenter_email'
                id='presenter_email'
                className='form-control'
              />
              <label htmlFor='presenter_email'>Presenter email</label>
            </div>
            <div className='form-floating mb-3'>
              <input
                onChange={handleInputChange}
                value={formData.company_name}
                placeholder='company name'
                required
                type='text'
                name='company_name'
                id='company_name'
                className='form-control'
              />
              <label htmlFor='title'>Company name</label>
            </div>
            <div className='form-floating mb-3'>
              <input
                onChange={handleInputChange}
                value={formData.title}
                placeholder='title'
                required
                type='text'
                name='title'
                id='title'
                className='form-control'
              />
              <label htmlFor='title'>Title</label>
            </div>
            <div className='mb-3'>
              <label htmlFor='synposis'>Synopsis</label>
              <textarea
                onChange={handleInputChange}
                value={formData.synopsis}
                // placeholder='Synopsis'
                required
                name='synopsis'
                id='synopsis'
                className='form-control'
                rows='3'
              ></textarea>
            </div>
            <div className='mb-3'>
              <select
                onChange={handleInputChange}
                value={formData.conference}
                required
                id='conference'
                name='conference'
                className='form-select'
              >
                <option value=''>Choose a conference</option>
                {conferences.map((conference) => {
                  return (
                    <option key={conference.id} value={conference.id}>
                      {conference.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className='btn btn-outline-success'>Create</button>
            <button
              className='btn btn-outline-danger'
              id='resetForm'
              type='reset'
            >
              Reset
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default PresentationForm();
