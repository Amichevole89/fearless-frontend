import React from "react";

const NavItem = ({ item }) => {
  return (
    <li className='nav-item'>
      <a className='nav-link' aria-current='page' href='http://localhost:3000'>
        Home
      </a>
    </li>
  );
};

export default NavItem;
