import React, { useEffect, useState } from "react";

const ConferenceForm = () => {
  const initialState = {
    name: "",
    starts: "",
    ends: "",
    description: "",
    max_presentations: "",
    max_attendees: "",
    location: "",
  };
  const [locations, setLocations] = useState([]);
  const [formData, setFormData] = useState(...initialState);
  const getData = async () => {
    const url = "http://localhost:8000/api/locations/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const conferenceURL = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: { "Content-Type": "application/json" },
    };
    const response = await fetch(conferenceURL, fetchConfig);
    if (response.ok) {
      setFormData(...initialState);
    }
  };
  
  const handleInputChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <div className='row'>
      <div className='offset-3 col-6'>
        <div className='shadow p-4 mt-4'>
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id='create-conference-form'>
            <div className='form-floating mb-3'>
              <input
                onChange={handleInputChange}
                value={formData.name}
                placeholder='name'
                required
                type='text'
                name='name'
                id='name'
                className='form-control'
              />
              <label htmlFor='name'>Name</label>
            </div>
            <div className='form-floating mb-3'>
              <input
                onChange={handleInputChange}
                value={formData.starts}
                placeholder='starts'
                required
                type='date'
                name='starts'
                id='starts'
                className='form-control'
              />
              <label htmlFor='starts'>Starts</label>
            </div>
            <div className='form-floating mb-3'>
              <input
                onChange={handleInputChange}
                value={formData.ends}
                placeholder='ends'
                required
                type='date'
                name='ends'
                id='ends'
                className='form-control'
              />
              <label htmlFor='ends'>Ends</label>
            </div>
            <div className='mb-3'>
              <label htmlFor='description'>Description</label>
              <textarea
                onChange={handleInputChange}
                value={formData.description}
                placeholder='description'
                required
                name='description'
                id='description'
                className='form-control'
                rows='3'
              ></textarea>
            </div>
            <div className='form-floating mb-3'>
              <input
                onChange={handleInputChange}
                value={formData.max_presentations}
                placeholder='max_presentations'
                required
                type='number'
                name='max_presentations'
                id='max_presentations'
                className='form-control'
              />
              <label htmlFor='max_presentations'>Max Presentations</label>
            </div>
            <div className='form-floating mb-3'>
              <input
                onChange={handleInputChange}
                value={formData.max_attendees}
                placeholder='max_attendees'
                required
                type='number'
                name='max_attendees'
                id='max_attendees'
                className='form-control'
              />
              <label htmlFor='max_attendees'>Max Attendees</label>
            </div>

            <div className='mb-3'>
              <select
                onChange={handleInputChange}
                required
                value={formData.location}
                id='location'
                name='location'
                className='form-select'
              >
                <option value=''>Choose a location</option>
                {locations.map((location) => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className='btn btn-outline-success'>Create</button>
            <button
              className='btn btn-outline-danger'
              id='resetForm'
              type='reset'
            >
              Reset
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};
export default ConferenceForm();
