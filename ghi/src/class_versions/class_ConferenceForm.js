import React from "react";
class ConferenceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      starts: "",
      ends: "",
      description: "",
      max_presentations: "",
      max_attendees: "",
      locations: [],
    };
    // this.handleNameChange = this.handleNameChange.bind(this);
    // this.handleStartsChange = this.handleStartsChange.bind(this);
    // this.handleEndsChange = this.handleEndsChange.bind(this);
    // this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    // this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
    // this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
    // this.handleLocationChange = this.handleLocationChange.bind(this);
    // this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  //   handleNameChange(event){
  //     const value = event.target.value;
  //     this.setState({name: value})
  //   }
  //   handleStartsChange(event){
  //     const value = event.target.value;
  //     this.setState({starts: value})
  //   }
  //   handleEndsChange(event){
  //     const value = event.target.value;
  //     this.setState({ends: value})
  //   }
  //   handleDescriptionChange(event){
  //     const value = event.target.value;
  //     this.setState({description: value})
  //   }
  //   handleMaxPresentationsChange(event){
  //     const value = event.target.value;
  //     this.setState({max_presentations: value})
  //   }
  //   handleMaxAttendeesChange(event){
  //     const value = event.target.value;
  //     this.setState({max_attendees: value})
  //   }
  //   handleLocationChange(event){
  //     const value = event.target.value;
  //     this.setState({location: value})
  //   }
  handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ ...this.state, [name]: value });
    // const newState = this.state;
    // newState[name] = value;
    // this.setState(newState);
  };
  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    // data.max_attendees = data.maxAttendees;
    // data.max_presentations = data.maxPresentations;
    data.locations = data.location;
    // delete data.maxAttendees;
    // delete data.maxPresentations;
    delete data.locations;

    console.log(data);

    const conferenceUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" },
    };
    const response = await fetch(conferenceUrl, fetchConfig);
    // console.log(response);
    if (response.ok) {
      // const newConference = await response.json();

      const cleared = {
        name: "",
        starts: "",
        ends: "",
        description: "",
        max_presentations: "",
        max_attendees: "",
        location: "",
      };
      this.setState(cleared);
    }
  }
  async componentDidMount() {
    const url = "http://localhost:8000/api/locations/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ locations: data.locations });
    }
  }
  render() {
    return (

        <div className='row'>
          <div className='offset-3 col-6'>
            <div className='shadow p-4 mt-4'>
              <h1>Create a new conference</h1>
              <form onSubmit={this.handleSubmit} id='create-conference-form'>
                <div className='form-floating mb-3'>
                  <input
                    onChange={this.handleInputChange}
                    value={this.state.name}
                    placeholder='name'
                    required
                    type='text'
                    name='name'
                    id='name'
                    className='form-control'
                  />
                  <label htmlFor='name'>Name</label>
                </div>
                <div className='form-floating mb-3'>
                  <input
                    onChange={this.handleInputChange}
                    value={this.state.starts}
                    placeholder='starts'
                    required
                    type='date'
                    name='starts'
                    id='starts'
                    className='form-control'
                  />
                  <label htmlFor='starts'>Starts</label>
                </div>
                <div className='form-floating mb-3'>
                  <input
                    onChange={this.handleInputChange}
                    value={this.state.ends}
                    placeholder='ends'
                    required
                    type='date'
                    name='ends'
                    id='ends'
                    className='form-control'
                  />
                  <label htmlFor='ends'>Ends</label>
                </div>
                <div className='mb-3'>
                  <label htmlFor='description'>Description</label>
                  <textarea
                    onChange={this.handleInputChange}
                    value={this.state.description}
                    placeholder='description'
                    required
                    name='description'
                    id='description'
                    className='form-control'
                    rows='3'
                  ></textarea>
                </div>
                <div className='form-floating mb-3'>
                  <input
                    onChange={this.handleInputChange}
                    value={this.state.max_presentations}
                    placeholder='max_presentations'
                    required
                    type='number'
                    name='max_presentations'
                    id='max_presentations'
                    className='form-control'
                  />
                  <label htmlFor='max_presentations'>Max Presentations</label>
                </div>
                <div className='form-floating mb-3'>
                  <input
                    onChange={this.handleInputChange}
                    value={this.state.max_attendees}
                    placeholder='max_attendees'
                    required
                    type='number'
                    name='max_attendees'
                    id='max_attendees'
                    className='form-control'
                  />
                  <label htmlFor='max_attendees'>Max Attendees</label>
                </div>

                <div className='mb-3'>
                  <select
                    onChange={this.handleInputChange}
                    required
                    value={this.state.location}
                    id='location'
                    name='location'
                    className='form-select'
                  >
                    <option value=''>Choose a location</option>
                    {this.state.locations.map((location) => {
                      return (
                        <option key={location.id} value={location.id}>
                          {location.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <button className='btn btn-outline-success'>Create</button>
                <button
                  className='btn btn-outline-danger'
                  id='resetForm'
                  type='reset'
                >
                  Reset
                </button>
              </form>
            </div>
          </div>
        </div>

    );
  }
}

export default ConferenceForm;
