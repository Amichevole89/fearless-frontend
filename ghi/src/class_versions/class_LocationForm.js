import React, { useState, useEffect } from "react";
class LocationForm extends React.Component {
  constructor(props) {
    super(props);
    formData = {
      name: "",
      room_count: "",
      city: "",
      states: [],
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ ...formData, [name]: value });
  };
  handlePythonCase = () => {};

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...formData };
    delete data.states;

    const locationUrl = "http://localhost:8000/api/locations/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();

      const cleared = {
        name: "",
        room_count: "",
        city: "",
        state: "",
      };
      this.setState(cleared);
    }
  }
  async componentDidMount() {
    const url = "http://localhost:8000/api/states/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ states: data.states });
    }
  }
  render() {
    return (
      <div className='row'>
        <div className='offset-3 col-6'>
          <div className='shadow p-4 mt-4'>
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id='create-location-form'>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleFormChange}
                  value={formData.name}
                  placeholder='Name'
                  required
                  type='text'
                  name='name'
                  id='name'
                  className='form-control'
                />
                <label htmlFor='name'>Name</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleFormChange}
                  value={formData.room_count}
                  placeholder='Room count'
                  required
                  type='number'
                  name='room_count'
                  id='room_count'
                  className='form-control'
                />
                <label htmlFor='room_count'>Room count</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleFormChange}
                  value={formData.city}
                  placeholder='City'
                  required
                  type='text'
                  name='city'
                  id='city'
                  className='form-control'
                />
                <label htmlFor='city'>City</label>
              </div>
              <div className='mb-3'>
                <select
                  onChange={handleFormChange}
                  value={formData.state}
                  required
                  id='state'
                  name='state'
                  className='form-select'
                >
                  <option value=''>Choose a state</option>
                  {states.map((state) => {
                    return (
                      <option
                        key={state.abbreviation}
                        value={state.abbreviation}
                      >
                        {state.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className='btn btn-outline-success'>Create</button>
              <button
                className='btn btn-outline-danger'
                id='resetForm'
                type='reset'
              >
                Reset
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default LocationForm;
