import React from "react";
class PresentationForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      presenter_name: "",
      presenter_email: "",
      company_name: "",
      title: "",
      synopsis: "",
      conferences: [],
    };
  }
  handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ ...this.state, [name]: value });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = { ...this.state };
    // data.conference = data.conferences;
    delete data.conferences;

    const presentationUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" },
    };
    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
      const cleared = {
        presenter_name: "",
        presenter_email: "",
        company_name:"",
        title: "",
        synopsis: "",
        conference: "",
      };
      this.setState(cleared);
    }
  };
  async componentDidMount() {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      // console.log(data);
      this.setState({ conferences: data.conferences });
    }
  }
  render() {
    return (
      <div className='row'>
        <div className='offset-3 col-6'>
          <div className='shadow p-4 mt-4'>
            <h1>Create a new presentation</h1>
            <form onSubmit={this.handleSubmit} id='create-presentation-form'>
              <div className='form-floating mb-3'>
                <input
                  onChange={this.handleInputChange}
                  value={this.state.presenter_name}
                  placeholder='presenter_name'
                  required
                  type='text'
                  name='presenter_name'
                  id='presenter_name'
                  className='form-control'
                />
                <label htmlFor='presenter_name'>Presenter name</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={this.handleInputChange}
                  value={this.state.presenter_email}
                  placeholder='presenter_email'
                  required
                  type='email'
                  name='presenter_email'
                  id='presenter_email'
                  className='form-control'
                />
                <label htmlFor='presenter_email'>Presenter email</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={this.handleInputChange}
                  value={this.state.company_name}
                  placeholder='company name'
                  required
                  type='text'
                  name='company_name'
                  id='company_name'
                  className='form-control'
                />
                <label htmlFor='title'>Company name</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={this.handleInputChange}
                  value={this.state.title}
                  placeholder='title'
                  required
                  type='text'
                  name='title'
                  id='title'
                  className='form-control'
                />
                <label htmlFor='title'>Title</label>
              </div>
              <div className='mb-3'>
                <label htmlFor='synposis'>Synopsis</label>
                <textarea
                  onChange={this.handleInputChange}
                  value={this.state.synopsis}
                  // placeholder='Synopsis'
                  required
                  name='synopsis'
                  id='synopsis'
                  className='form-control'
                  rows='3'
                ></textarea>
              </div>
              <div className='mb-3'>
                <select
                  onChange={this.handleInputChange}
                  value={this.state.conference}
                  required
                  id='conference'
                  name='conference'
                  className='form-select'
                >
                  <option value=''>Choose a conference</option>
                  {this.state.conferences.map((conference) => {
                    return (
                      <option key={conference.id} value={conference.id}>
                        {conference.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className='btn btn-outline-success'>Create</button>
              <button
                className='btn btn-outline-danger'
                id='resetForm'
                type='reset'
              >
                Reset
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default PresentationForm;
