import React, { useState, useEffect } from "react";

const AttendConferenceForm = () => {
  const initialState = {
    name: "",
    email: "",
    conference: "",
  };
  const [conferences, setConferences] = useState([]);
  const [formData, setFormData] = useState(...initialState);
  const [hasSignedUp, setHasSignedUp] = useState(false);

  const getData = async () => {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const locationURL = `http://localhost:8000${formData.conference}attendees/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: { "Content-Type": "application/json" },
    };
    const response = await fetch(locationURL, fetchConfig);

    if (response.ok) {
      setFormData(...initialState);
      setHasSignedUp(true);
    }
  };

  const handleInputChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };
  const formClasses = !hasSignedUp ? "" : "d-none";
  const messageClasses = !hasSignedUp
    ? "alert alert-success d-none mb-0"
    : "alert alert-success mb-0";
  let spinnerClasses =
    conferences.length > 0
      ? "d-flex justify-content-center mb-3 d-none"
      : "d-flex justify-content-center mb-3";
  let dropdownClasses =
    conferences.length > 0 ? "form-select" : "form-select d-none";

  return (
    <div className='my-5 container'>
      <div className='row'>
        <div className='col col-sm-auto'>
          <img
            width='300'
            className='bg-white rounded shadow d-block mx-auto mb-4'
            src='/logo.svg'
            alt='unable to load'
          />
        </div>
        <div className='col'>
          <div className='card shadow'>
            <div className='card-body'>
              <form
                className={formClasses}
                onSubmit={handleSubmit}
                id='create-attendee-form'
              >
                <h1 className='card-title'>It's Conference Time!</h1>
                <p className='mb-3'>
                  Please choose which conference you'd like to attend.
                </p>
                <div
                  className={spinnerClasses}
                  id='loading-conference-spinner'
                >
                  <div className='spinner-grow text-secondary' role='status'>
                    <span className='visually-hidden'>Loading...</span>
                  </div>
                </div>
                <div className='mb-3'>
                  <select
                    onChange={handleInputChange}
                    value={conferences}
                    name='conference'
                    id='conference'
                    className={dropdownClasses}
                    required
                  >
                    <option value=''>Choose a conference</option>
                    {conferences.map((conference) => {
                      return (
                        <option key={conference.href} value={conference.href}>
                          {conference.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <p className='mb-3'>Now, tell us about yourself.</p>
                <div className='row'>
                  <div className='col'>
                    <div className='form-floating mb-3'>
                      <input
                        required
                        onChange={handleInputChange}
                        placeholder='Your full name'
                        type='text'
                        id='name'
                        name='name'
                        className='form-control'
                      />
                      <label htmlFor='name'>Your full name</label>
                    </div>
                  </div>
                  <div className='col'>
                    <div className='form-floating mb-3'>
                      <input
                        required
                        onChange={handleInputChange}
                        placeholder='Your email address'
                        type='email'
                        id='email'
                        name='email'
                        className='form-control'
                      />
                      <label htmlFor='email'>Your email address</label>
                    </div>
                  </div>
                </div>
                <button className='btn btn-lg btn-primary'>I'm going!</button>
              </form>
              <div className={messageClasses} id='success-message'>
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AttendConferenceForm();
