import React, { useState, useEffect } from "react";
const LocationForm = () => {
  const initialState = {
    name: "",
    room_count: "",
    city: "",
    state: "",
  };
  const [states, setStates] = useState([]);
  const [formData, setFormData] = useState(...initialState);

  const getData = async () => {
    const url = "http://localhost:8000/api/states/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setStates(data.states);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const locationURL = "http://localhost:8000/api/locations/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: { "Content-Type": "application/json" },
    };
    const response = await fetch(locationURL, fetchConfig);
    if (response.ok) {
      setFormData(...initialState);
    }
  };
  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className='row'>
      <div className='offset-3 col-6'>
        <div className='shadow p-4 mt-4'>
          <h1>Create a new location</h1>
          <form onSubmit={handleSubmit} id='create-location-form'>
            <div className='form-floating mb-3'>
              <input
                onChange={handleFormChange}
                value={formData.name}
                placeholder='Name'
                required
                type='text'
                name='name'
                id='name'
                className='form-control'
              />
              <label htmlFor='name'>Name</label>
            </div>
            <div className='form-floating mb-3'>
              <input
                onChange={handleFormChange}
                value={formData.room_count}
                placeholder='Room count'
                required
                type='number'
                name='room_count'
                id='room_count'
                className='form-control'
              />
              <label htmlFor='room_count'>Room count</label>
            </div>
            <div className='form-floating mb-3'>
              <input
                onChange={handleFormChange}
                value={formData.city}
                placeholder='City'
                required
                type='text'
                name='city'
                id='city'
                className='form-control'
              />
              <label htmlFor='city'>City</label>
            </div>
            <div className='mb-3'>
              <select
                onChange={handleFormChange}
                value={formData.state}
                required
                id='state'
                name='state'
                className='form-select'
              >
                <option value=''>Choose a state</option>
                {states.map((state) => {
                  return (
                    <option
                      key={state.abbreviation}
                      value={state.abbreviation}
                    >
                      {state.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className='btn btn-outline-success'>Create</button>
            <button
              className='btn btn-outline-danger'
              id='resetForm'
              type='reset'
            >
              Reset
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default LocationForm;
