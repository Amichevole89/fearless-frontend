import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const ConferenceColumn = (props) => {
  return (
    <div className='col'>
      {props.list.map((data) => {
        const conference = data.conference;
        return (
          <div key={conference.href} className='card mb-3 shadow'>
            <img
              src={conference.location.picture_url}
              className='card-img-top'
              alt=''
            />
            <div className='card-body'>
              <h5 className='card-title'>{conference.name}</h5>
              <h6 className='card-subtitle mb-2 text-muted'>
                {conference.location.name}
              </h6>
              <p className='card-text'>{conference.description}</p>
            </div>
            <div className='card-footer'>
              {new Date(conference.starts).toLocaleDateString()}-
              {new Date(conference.ends).toLocaleDateString()}
            </div>
          </div>
        );
      })}
    </div>
  );
};

const MainPage = () => {
  const initialState = {
    conferenceColumns: [[], [], []],
  };
  const [conferences, setConferences] = useState([]);
  const [columns, setColumns] = useState(...initialState);

  const getData = async () => {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  const confDetails = async () => {
    const requests = [];
    try {
      conferences.map((conference) => {
        const detailURL = `http://localhost:8000${conference.href}`;
        requests.push(fetch(detailURL));
      });

      const responses = await fetch(requests);
      const conferenceColumns = [[], [], []];
      let i = 0;

      responses.map(async (conference) => {
        if (conference.ok) {
          const details = await conference.json();
          conferenceColumns[i].push(details);
          i++;
          if (i > 2) {
            i = 0;
          } else {
            console.error(conference);
          }
        }
      });

      setColumns(...conferenceColumns);
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <>
      <div className='px-4 py-5 my-5 mt-0 text-center bg-info'>
        <img
          className='bg-white rounded shadow d-block mx-auto mb-4'
          src='/logo.svg'
          alt=''
          width='600'
        />
        <h1 className='display-5 fw-bold'>Conference GO!</h1>
        <div className='col-lg-6 mx-auto'>
          <p className='lead mb-4'>
            The only resource you'll ever need to plan an run your in-person or
            virtual conference for thousands of attendees and presenters.
          </p>
          <div className='d-grid gap-2 d-sm-flex justify-content-sm-center'>
            <Link
              to='/attendees/new'
              className='btn btn-primary btn-lg px-4 gap-3'
            >
              Attend a conference
            </Link>
          </div>
        </div>
      </div>
      <div className='container'>
        <h2>Upcoming conferences</h2>
        <div className='row'>
          {columns.map((conferenceList, index) => {
            return <ConferenceColumn key={index} list={conferenceList} />;
          })}
        </div>
      </div>
    </>
  );
};

export default MainPage();
